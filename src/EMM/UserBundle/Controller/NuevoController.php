<?php

namespace EMM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class NuevoController extends Controller
{
    public function indexAction()
    {
        return new Response('Éste es un nuevo bundle');
    }

    public function articlesAction($pagina){
        //return new Response('Éste es el artículos número: ' .$pagina);
        return $this->render('EMMUserBundle:Default:articulos.html.twig', array('pagina' => $pagina));
    }
}
