<?php

namespace EMM\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EMM\UserBundle\Entity\Usuarios;
use EMM\UserBundle\Form\UsuariosType;

class UserController extends Controller
{
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('EMMUserBundle:Usuarios')->findAll();

        /*
        $res = 'Lista de usuarios <br /> <br />';
        foreach ($usuarios as $user){
            $res .= 'Usuario: ' . $user->getUserName() . ' : Email: ' . $user->getEmail() .'<br/>';
        }
        return new Response($res);
        */

        return $this->render('EMMUserBundle:Usuarios:index.html.twig', array('usuarios' => $usuarios));

    }

    public function addAction()
    {
        $usuarios = new Usuarios();
        $form = $this->createCreateForm($usuarios);

        return $this->render('EMMUserBundle:Usuarios:add.html.twig', array('form' => $form->createView()));
    }

    private function createCreateForm(Usuarios $entity){
        $form = $this->createForm(new UsuariosType(), $entity, array(
            'action' => $this->generateUrl('emm_user_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request){
        $Usuarios = new Usuarios();
        $form = $this->createCreateForm($Usuarios);
        $form->handleRequest($request);

        if($form->isValid()){

            $password = $form->get('password')->getData();
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($Usuarios, $password);
            $Usuarios->setPassword($encoded);

            $em = $this->getDoctrine()->getManager();
            $em->persist($Usuarios);
            $em->flush();

            $this->addFlash('mensaje', 'El usuario fue creado correctamente.');

            return $this->redirectToRoute('emm_user_index');
        }

        return $this->render('EMMUserBundle:Usuarios:add.html.twig', array('form' => $form->createView()));

    }

    public function editAction($id)
    {
        return new Response('Soy edit');
    }

    public function viewAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('EMMUserBundle:Usuarios');
        $usuario = $repository->find($id);
        return new Response('Usuario: ' .$usuario->getUsername());
    }

    public function deleteAction($id)
    {
        return new Response('Soy delete');
    }

    public function pruebaAction(){
        return $this->render('EMMUserBundle:Usuarios:prueba.html.twig');
    }
}
