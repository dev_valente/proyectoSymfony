<?php

namespace EMM\UserBundle\Form;

use function Sodium\add;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuariosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('username')
            ->add('lastName')
            ->add('email', 'email')
            ->add('password', 'password')
            ->add('role')
            ->add('isActive', 'checkbox')
//            ->add('createdAt')
//            ->add('updatedAt')
            ->add('save', 'submit', array('label' => 'Save user'))
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EMM\UserBundle\Entity\Usuarios'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'emm_userbundle_usuarios';
    }


}
